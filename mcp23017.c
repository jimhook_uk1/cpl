/***************************************************************************************************
 * Name:    MCP23017 Driver
 * File:    mcp23017.c
 * Author:  RDTek
 * Date:    11/02/17
 *
 * Copyright:
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 11/02/17 | RDTek | Created.
 *
 * Description:
  **************************************************************************************************/

/**************************************************************************************** Include */
#include "i2c.h"
#include "mcp23017.h"

/******************************************************************************* Global Variables */

/*********************************************************************** Local Macros and Defines */

/********************************************************************************* Local TypeDefs */

/*********************************************************************** Local Constants (static) */

/*********************************************************************** Local Variables (static) */

/********************************************************************** Local Function Prototypes */

/********************************************************************************* mcp23017_init ***
 * Name:        Initialise LED Interface
 * Parameters:  None
 * Returns:     None
 * Description: Initialises the I2c interface
 **************************************************************************************************/
void mcp23017_init(void) {
    
}

/******************************************************************************* mcp23017_rdData ***
 * Name:        Read Data
 * Parameters:  buf[]
 *              ledRegister
 *              nOfB
 * Returns:     None
 * Description: Gets nOfB of data from the device at slaveAddress starting at address.
 **************************************************************************************************/
void mcp23017_rdData(uint8_t buf[], uint8_t ledRegister, uint8_t nOfB) {
    i2c_read(buf[0], ledRegister, buf + ledRegister + 1, nOfB);
}

/******************************************************************************* mcp23017_wrData ***
 * Name:        Write Data
 * Parameters:  buf[]
 *              ledRegister
 *              nOfB
 * Returns:     None
 * Description: Transmits nOfB of data from buf to device at slaveAddress starting at address.
 **************************************************************************************************/
bool mcp23017_wrData(uint8_t buf[], uint8_t ledRegister, uint8_t nOfB) {
    return i2c_write(buf[0], ledRegister, buf + ledRegister + 1, nOfB);
}
/*
 * End of Workfile: main.c
 */

