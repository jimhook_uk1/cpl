/***************************************************************************************************
 * Name:    TLC59116 Driver
 * File:    led.c
 * Author:  RDTek
 * Date:    15/11/15
 *
 * Copyright:
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 15/11/15 | RDTek | Created.
 *
 * Description:
 * Simple I2C Master
 **************************************************************************************************/

/**************************************************************************************** Include */
#include "i2c.h"
#include "led.h"

/******************************************************************************* Global Variables */

/*********************************************************************** Local Macros and Defines */

/********************************************************************************* Local TypeDefs */

/*********************************************************************** Local Constants (static) */

/*********************************************************************** Local Variables (static) */

/********************************************************************** Local Function Prototypes */

/************************************************************************************** led_init ***
 * Name:        Initialise LED Interface
 * Parameters:  clockDiv
 * Returns:     None
 * Description: Initialises the I2c interface
 **************************************************************************************************/
void led_init(void) {
    
}

/************************************************************************************ led_rdData ***
 * Name:        Read Data
 * Parameters:  buf[]
 *              ledRegister
 *              nOfB
 * Returns:     None
 * Description: Gets nOfB of data from the device at slaveAddress starting at address.
 **************************************************************************************************/
void led_rdData(uint8_t buf[], uint8_t ledRegister, uint8_t nOfB) {
    i2c_read(buf[0], ledRegister, buf + ledRegister + 1, nOfB);
}

/************************************************************************************ led_wrData ***
 * Name:        Write Data
 * Parameters:  buf[]
 *              ledRegister
 *              nOfB
 * Returns:     None
 * Description: Transmits nOfB of data from buf to device at slaveAddress starting at address.
 **************************************************************************************************/
bool led_wrData(uint8_t buf[], uint8_t ledRegister, uint8_t nOfB) {
    return i2c_write(buf[0], ledRegister, buf + ledRegister + 1, nOfB);
}

