#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
# default configuration
CND_ARTIFACT_DIR_default=dist/default/production
CND_ARTIFACT_NAME_default=CPLCtrl.X.production.hex
CND_ARTIFACT_PATH_default=dist/default/production/CPLCtrl.X.production.hex
CND_PACKAGE_DIR_default=${CND_DISTDIR}/default/package
CND_PACKAGE_NAME_default=cplctrl.x.tar
CND_PACKAGE_PATH_default=${CND_DISTDIR}/default/package/cplctrl.x.tar
# CPL2 configuration
CND_ARTIFACT_DIR_CPL2=dist/CPL2/production
CND_ARTIFACT_NAME_CPL2=CPLCtrl.X.production.hex
CND_ARTIFACT_PATH_CPL2=dist/CPL2/production/CPLCtrl.X.production.hex
CND_PACKAGE_DIR_CPL2=${CND_DISTDIR}/CPL2/package
CND_PACKAGE_NAME_CPL2=cplctrl.x.tar
CND_PACKAGE_PATH_CPL2=${CND_DISTDIR}/CPL2/package/cplctrl.x.tar
