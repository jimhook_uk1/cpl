/***************************************************************************************************
 * Name: main
 * Workfile: main.c
 * Author: RDTek
 * Date: 08/05/17
 *
 * Copyright:  RD Technical Solutions
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 04/02/17 | RDTek | Created.
 * 08/05/17 | RDTek | Updated as per CPL1_8.xlsx
 * 21/05/17 | RDTek | Updated as Per CPL1_10.xlsx
 * 02/06/17 | RDTek | Corrected mistakes + updated per CPL1_11.xlsx
 * 07/07/17 | RDTek | Changes marked *5*
 * 21/07/17 | RDTek | Updated for use with new pcb. (CPL2_BUILD)
 * 25/07/17 | RDTek | Added eng not running check back into engine start *7*
 * 30/08/17 | RDTek | Added line to actually transfer data from U19 *8*
 * 04/09/17 | RDTek | P18-3 is now beacons output. P18-1 includes KL15 input *9*
 * 05/09/17 | RDTek | Added P9-1 legs warning functionality and corrected legs stowed *10*
 * 10/12/17 | RDTek | Changed ALT-W input to an interrupt *11*
 *
 * Description:
 * Fosc=32MHz,Fcy=16MHz,Fp=16MHz
 **************************************************************************************************/


/**************************************************************************************** Include */
#include <xc.h>
#include <stdbool.h>
#include <stdint.h>
#include "i2c.h"
#include "led.h"
#include "mcp23017.h"

/******************************************************************************* Global Variables */

/*********************************************************************** Local Macros and Defines */
//#define TESTBOX
#define kl15V2Bits(x)       ((x) * 54)
#define FOSC                64u
#define I2C_BRG             255 //37
/* NOTE: The settings assume a 16MHz peripheral clock Fp */
#define RTC_SCALE           3u
#define RTC_PERIOD          624u

/* TLC59116 */
#define BLINK_FREQ          23 /* 1 second */
#define TLC59116_ADDRESS    LED_I2C_BASE_ADD
#define TLC59116_NUM_REG    24

/* MCP23017 */
#define MCP23017U16_ADDRESS MCP23017_I2C_BASE_ADD + 0x0C
#ifdef CPL2_BUILD
#define MCP23017U19_ADDRESS MCP23017_I2C_BASE_ADD + 0x0E
#endif
#define MCP23017U2_ADDRESS  MCP23017_I2C_BASE_ADD + 0x08
#define MCP23017U4_ADDRESS  MCP23017_I2C_BASE_ADD + 0x04
#define MCP23017_NUM_REG    22

#ifdef CPL2_BUILD
#define ENBL_STATE_IDLE         0
#define ENBL_STATE_LED_ENABLE   1
#define ENBL_STATE_U2_ENABLE    2
#define ENBL_STATE_U4_ENABLE    3
#define ENBL_STATE_U16_ENABLE   4
#define ENBL_STATE_U19_ENABLE   5
#define ENBL_STATE_WAIT         6
#else
#define ENBL_STATE_IDLE         0
#define ENBL_STATE_LED_ENABLE   1
#define ENBL_STATE_U2_ENABLE    2
#define ENBL_STATE_U4_ENABLE    3
#define ENBL_STATE_U16_ENABLE   4
#define ENBL_STATE_WAIT         5
#endif
#define ENG_NOT_RUNNING_DELAY   100  /* 100msecs */ /* *11* Changed */
#define FLAG_ENG_RUNNING        0x01
#define FLAG_IGNITION           0x02
#define FLAG_LEGS_UNSTOWED      0x04
#define FLAG_LEG_STOWED         0x08
#define FLAG_ESTOP              0x10
#define FLAG_LEGS_DOWN          0x20                /* *5* Changed */
#define FLAG_SYS_ENABLE         0x40                /* *5* Added */
#define FLAG_BOOM_HOME          0x80                /* *5* Added */
#define FLAG_LEGS_DOWN_LATCHED  0x01                /* *5* Added */
#define KL15_OFF                kl15V2Bits(2.0)
#define KL15_ON                 kl15V2Bits(4.0)
#define OP_STATE_INIT           0
#define OP_STATE_ENBL           1
#define OP_STATE_RUNNING        3

#ifdef CPL2_BUILD
#define RUN_STATE_IDLE          0
#define RUN_STATE_GET_U2_IP     1
#define RUN_STATE_GET_U4_IP     2
#define RUN_STATE_GET_U16_IP    3
#define RUN_STATE_GET_U19_IP    4
#define RUN_STATE_PROCESS       5
#define RUN_STATE_PUT_U2_OP     6
#define RUN_STATE_PUT_LED_OP    7
#define RUN_STATE_WAIT          8
#else
#define RUN_STATE_IDLE          0
#define RUN_STATE_GET_U2_IP     1
#define RUN_STATE_GET_U4_IP     2
#define RUN_STATE_GET_U16_IP    3
#define RUN_STATE_PROCESS       4
#define RUN_STATE_PUT_U2_OP     5
#define RUN_STATE_PUT_LED_OP    6
#define RUN_STATE_WAIT          7
#endif

#define bIsEngRunning()         ((u8Flags & FLAG_ENG_RUNNING) != 0)
#define vClrEngRunning()        (u8Flags &= ~FLAG_ENG_RUNNING)
#define vSetEngRunning()        (u8Flags |= FLAG_ENG_RUNNING)
#define bIsKL15Set()            ((u8Flags & FLAG_IGNITION) != 0)
#define vClrKL15()              (u8Flags &= ~FLAG_IGNITION)
#define vSetKL15()              (u8Flags |= FLAG_IGNITION)
#define bIsEStopSet()           ((u8Flags & FLAG_ESTOP) != 0)
#define vClrEStop()             (u8Flags &= ~FLAG_ESTOP)
#define vSetEStop()             (u8Flags |= FLAG_ESTOP)
#define bIsLegsUnstowed()       ((u8Flags & FLAG_LEGS_UNSTOWED) != 0)
#define vClrLegsUnstowed()      (u8Flags &= ~FLAG_LEGS_UNSTOWED)
#define vSetLegsUnstowed()      (u8Flags |= FLAG_LEGS_UNSTOWED)
#define bAreLegsDown()          ((u8Flags & FLAG_LEGS_DOWN) != 0)   /* *5* Changed */
#define vClrLegsDown()          (u8Flags &= ~FLAG_LEGS_DOWN)        /* *5* Changed */
#define vSetLegsDown()          (u8Flags |= FLAG_LEGS_DOWN)         /* *5* Changed */
#define bIsSysEnabled()         ((u8Flags & FLAG_SYS_ENABLE) != 0)  /* *5* Added */
#define vClrSysEnable()         (u8Flags &= ~FLAG_SYS_ENABLE)       /* *5* Added */
#define vSetSysEnable()         (u8Flags |= FLAG_SYS_ENABLE)        /* *5* Added */
#define bIsBoomHome()           ((u8Flags & FLAG_BOOM_HOME) != 0)   /* *5* Added */
#define vClrBoomHome()          (u8Flags &= ~FLAG_BOOM_HOME)        /* *5* Added */
#define vSetBoomHome()          (u8Flags |= FLAG_BOOM_HOME)         /* *5* Added */
#define bIsLegsDownLatched()    ((u8Flags2 & FLAG_LEGS_DOWN_LATCHED) != 0)   /* *5* Added */
#define vClrLegsDownLatched()   (u8Flags2 &= ~FLAG_LEGS_DOWN_LATCHED)        /* *5* Added */
#define vSetLegsDownLatched()   (u8Flags2 |= FLAG_LEGS_DOWN_LATCHED)         /* *5* Added */

/* Hardware Interfaces */
#define GPIOA               0
#define GPIOB               1
#define MASK_0              0x01u
#define MASK_1              0x02u
#define MASK_2              0x04u
#define MASK_3              0x08u
#define MASK_4              0x10u
#define MASK_5              0x20u
#define MASK_6              0x40u
#define MASK_7              0x80u
#define bIsSetP1_1()        ((u8aU4Gpio[GPIOB] & MASK_0) != 0)
#define bIsSetP1_2()        ((u8aU16Gpio[GPIOA] & MASK_1) != 0)
#define bIsSetP1_3()        ((u8aU4Gpio[GPIOB] & MASK_1) != 0)
#define bIsSetP1_4()        ((u8aU4Gpio[GPIOB] & MASK_7) != 0)
#define bIsSetP1_5()        ((u8aU4Gpio[GPIOB] & MASK_2) != 0)
#define bIsSetP1_6()        ((u8aU4Gpio[GPIOB] & MASK_6) != 0)
#define bIsSetP1_7()        ((u8aU4Gpio[GPIOB] & MASK_3) != 0)
#define bIsSetP1_8()        ((u8aU4Gpio[GPIOB] & MASK_5) != 0)
#define bIsSetP1_9()        ((u8aU16Gpio[GPIOA] & MASK_0) != 0)
#define bIsSetP1_10()       ((u8aU4Gpio[GPIOB] & MASK_4) != 0)
#define bIsSetP2_1()        ((u8aU2Gpio[GPIOB] & MASK_5) != 0)
#define bIsSetP2_2()        ((u8aU2Gpio[GPIOB] & MASK_4) != 0)
#define bIsSetP2_3()        ((u8aU2Gpio[GPIOB] & MASK_3) != 0)
#define bIsSetP2_4()        ((u8aU2Gpio[GPIOB] & MASK_0) != 0)
#define bIsSetP2_5()        ((u8aU2Gpio[GPIOB] & MASK_1) != 0)
#define bIsSetP2_6()        ((u8aU2Gpio[GPIOB] & MASK_2) != 0)
#ifdef CPL2_BUILD
#define bIsSetP3_5()        ((u8aU19Gpio[GPIOB] & MASK_0) != 0)
#define bIsSetP3_6()        ((u8aU19Gpio[GPIOB] & MASK_7) != 0)
#define bIsSetP3_7()        ((u8aU19Gpio[GPIOB] & MASK_1) != 0)
#define bIsSetP3_8()        ((u8aU19Gpio[GPIOB] & MASK_6) != 0)
#define bIsSetP3_9()        ((u8aU19Gpio[GPIOB] & MASK_2) != 0)
#define bIsSetP3_10()       ((u8aU19Gpio[GPIOB] & MASK_5) != 0)
#define bIsSetP3_11()       ((u8aU19Gpio[GPIOB] & MASK_3) != 0)
#define bIsSetP3_12()       ((u8aU19Gpio[GPIOB] & MASK_4) != 0)
#define bIsSetP3_13()       ((u8aU16Gpio[GPIOA] & MASK_4) != 0)
#define bIsSetP3_14()       ((u8aU16Gpio[GPIOA] & MASK_3) != 0)
#define bIsSetP3_15()       ((u8aU16Gpio[GPIOA] & MASK_5) != 0)
#define bIsSetP3_16()       ((u8aU16Gpio[GPIOA] & MASK_2) != 0)
#define bIsSetP3_17()       ((u8aU4Gpio[GPIOA] & MASK_0) != 0)
#define bIsSetP3_18()       ((u8aU4Gpio[GPIOA] & MASK_7) != 0)
#define bIsSetP3_19()       ((u8aU4Gpio[GPIOA] & MASK_1) != 0)
#define bIsSetP3_20()       ((u8aU4Gpio[GPIOA] & MASK_6) != 0)
#define bIsSetP3_21()       ((u8aU4Gpio[GPIOA] & MASK_2) != 0)
#define bIsSetP3_22()       ((u8aU4Gpio[GPIOA] & MASK_5) != 0)
#define bIsSetP3_23()       ((u8aU4Gpio[GPIOA] & MASK_3) != 0)
#define bIsSetP3_24()       ((u8aU4Gpio[GPIOA] & MASK_4) != 0)
#else
#define bIsSetP3_1()        ((u8aU16Gpio[GPIOA] & MASK_4) != 0)
#define bIsSetP3_2()        ((u8aU16Gpio[GPIOA] & MASK_3) != 0)
#define bIsSetP3_3()        ((u8aU16Gpio[GPIOA] & MASK_5) != 0)
#define bIsSetP3_4()        ((u8aU16Gpio[GPIOA] & MASK_2) != 0)
#define bIsSetP3_5()        ((u8aU4Gpio[GPIOA] & MASK_0) != 0)
#define bIsSetP3_6()        ((u8aU4Gpio[GPIOA] & MASK_7) != 0)
#define bIsSetP3_7()        ((u8aU4Gpio[GPIOA] & MASK_1) != 0)
#define bIsSetP3_8()        ((u8aU4Gpio[GPIOA] & MASK_6) != 0)
#define bIsSetP3_9()        ((u8aU4Gpio[GPIOA] & MASK_2) != 0)
#define bIsSetP3_10()       ((u8aU4Gpio[GPIOA] & MASK_5) != 0)
#define bIsSetP3_11()       ((u8aU4Gpio[GPIOA] & MASK_3) != 0)
#define bIsSetP3_12()       ((u8aU4Gpio[GPIOA] & MASK_4) != 0)
#endif
#define bIsSetP7_1()        ((u8aU16Gpio[GPIOB] & MASK_7) != 0)
#define bIsSetP7_2()        ((u8aU16Gpio[GPIOB] & MASK_6) != 0)
#define bIsSetP7_3()        ((u8aU16Gpio[GPIOB] & MASK_5) != 0)
#define bIsSetP7_4()        ((u8aU16Gpio[GPIOB] & MASK_4) != 0)
#define bIsSetP7_5()        ((u8aU16Gpio[GPIOB] & MASK_3) != 0)
#define bIsSetP7_6()        ((u8aU16Gpio[GPIOB] & MASK_2) != 0)
#define bIsSetP7_7()        ((u8aU16Gpio[GPIOB] & MASK_1) != 0)
#define bIsSetP7_8()        ((u8aU16Gpio[GPIOB] & MASK_0) != 0)
#define bIsSetP8_1()        (PORTAbits.RA0 != 0)
#define vClrEngStop()       (PORTBbits.RB9 = 0)
#define vClrHourEn()        (PORTBbits.RB15 = 0)
#define vClrHourPwr()       (PORTCbits.RC9 = 0)
#define vClrLed2()          (PORTBbits.RB14 = 0)
#define vClrP2_1()          (u8aU2Gpio[GPIOA] &= ~MASK_5)
#define vClrP2_2()          (u8aU2Gpio[GPIOA] &= ~MASK_4)
#define vClrP2_3()          (u8aU2Gpio[GPIOA] &= ~MASK_3)
#define vClrP2_4()          (u8aU2Gpio[GPIOA] &= ~MASK_0)
#define vClrP2_5()          (u8aU2Gpio[GPIOA] &= ~MASK_1)
#define vClrP2_6()          (u8aU2Gpio[GPIOA] &= ~MASK_2)
#if(0)
#define vClrP4_1()          (u8aPwm[8] = 0)
#define vClrP4_2()          (u8aPwm[9] = 0)
#define vClrP4_3()          (u8aPwm[10] = 0)
#define vClrP4_4()          (u8aPwm[11] = 0)
#define vClrP4_5()          (u8aPwm[12] = 0)
#define vClrP4_6()          (u8aPwm[13] = 0)
#define vClrP4_7()          (u8aPwm[14] = 0)
#define vClrP4_8()          (u8aPwm[15] = 0)
#define vClrP9_1()          (u8aPwm[0] = 0)
#define vClrP9_2()          (u8aPwm[1] = 0)
#define vClrP9_3()          (u8aPwm[2] = 0)
#define vClrP9_4()          (u8aPwm[3] = 0)
#define vClrP9_5()          (u8aPwm[4] = 0)
#define vClrP9_6()          (u8aPwm[5] = 0)
#define vClrP9_7()          (u8aPwm[6] = 0)
#define vClrP9_8()          (u8aPwm[7] = 0)
#else
#define vClrP4_1()          (u8aLed[2] &= ~0x01)
#define vClrP4_2()          (u8aLed[2] &= ~0x04)
#define vClrP4_3()          (u8aLed[2] &= ~0x10)
#define vClrP4_4()          (u8aLed[2] &= ~0x40)
#define vClrP4_5()          (u8aLed[3] &= ~0x01)
#define vClrP4_6()          (u8aLed[3] &= ~0x04)
#define vClrP4_7()          (u8aLed[3] &= ~0x10)
#define vClrP4_8()          (u8aLed[3] &= ~0x40)
#define vClrP9_1()          (u8aLed[0] &= ~0x01)
#define vClrP9_2()          (u8aLed[0] &= ~0x04)
#define vClrP9_3()          (u8aLed[0] &= ~0x10)
#define vClrP9_4()          (u8aLed[0] &= ~0x40)
#define vClrP9_5()          (u8aLed[1] &= ~0x01)
#define vClrP9_6()          (u8aLed[1] &= ~0x04)
#define vClrP9_7()          (u8aLed[1] &= ~0x10)
#define vClrP9_8()          (u8aLed[1] &= ~0x40)
#endif
#define vClrP10_2()         (PORTBbits.RB15 = 0)
#define vClrP10_3()         (PORTCbits.RC9 = 0)
#define vClrP15_2()         (PORTBbits.RB10 = 0)
#define vClrP17_2()         (PORTBbits.RB13 = 0)
#define vClrP18_1()         (PORTCbits.RC0 = 0)
#define vClrP18_2()         (PORTCbits.RC1 = 0)
#define vClrP18_3()         (PORTCbits.RC2 = 0)
#define vClrP18_4()         (PORTAbits.RA9 = 0)
#define vClrP18_5()         (PORTBbits.RB7 = 0)
#define vClrP18_7()         (PORTAbits.RA4 = 0)
#define vClrP18_8()         (PORTBbits.RB8 = 0)
#define vClrReset()         (PORTCbits.RC7 = 0)
#define vSetEngStop()       (PORTBbits.RB9 = 1)
#define vSetHourEn()        (PORTBbits.RB15 = 1)
#define vSetHourPwr()       (PORTCbits.RC9 = 1)
#define vSetLed2()          (PORTBbits.RB14 = 1)
#define vSetP2_1()          (u8aU2Gpio[GPIOA] |= MASK_5)
#define vSetP2_2()          (u8aU2Gpio[GPIOA] |= MASK_4)
#define vSetP2_3()          (u8aU2Gpio[GPIOA] |= MASK_3)
#define vSetP2_4()          (u8aU2Gpio[GPIOA] |= MASK_0)
#define vSetP2_5()          (u8aU2Gpio[GPIOA] |= MASK_1)
#define vSetP2_6()          (u8aU2Gpio[GPIOA] |= MASK_2)
#if(0)
#define vSetP4_1()          (u8aPwm[8] = 0xFF)
#define vSetP4_2()          (u8aPwm[9] = 0xFF)
#define vSetP4_3()          (u8aPwm[10] = 0xFF)
#define vSetP4_4()          (u8aPwm[11] = 0xFF)
#define vSetP4_5()          (u8aPwm[12] = 0xFF)
#define vSetP4_6()          (u8aPwm[13] = 0xFF)
#define vSetP4_7()          (u8aPwm[14] = 0xFF)
#define vSetP4_8()          (u8aPwm[15] = 0xFF)
#define vSetP9_1()          (u8aPwm[0] = 0xFF)
#define vSetP9_2()          (u8aPwm[1] = 0xFF)
#define vSetP9_3()          (u8aPwm[2] = 0xFF)
#define vSetP9_4()          (u8aPwm[3] = 0xFF)
#define vSetP9_5()          (u8aPwm[4] = 0xFF)
#define vSetP9_6()          (u8aPwm[5] = 0xFF)
#define vSetP9_7()          (u8aPwm[6] = 0xFF)
#define vSetP9_8()          (u8aPwm[7] = 0xFF)
#else
#define vSetP4_1()          (u8aLed[2] |= 0x01)
#define vSetP4_2()          (u8aLed[2] |= 0x04)
#define vSetP4_3()          (u8aLed[2] |= 0x10)
#define vSetP4_4()          (u8aLed[2] |= 0x40)
#define vSetP4_5()          (u8aLed[3] |= 0x01)
#define vSetP4_6()          (u8aLed[3] |= 0x04)
#define vSetP4_7()          (u8aLed[3] |= 0x10)
#define vSetP4_8()          (u8aLed[3] |= 0x40)
#define vSetP9_1()          (u8aLed[0] |= 0x01)
#define vSetP9_2()          (u8aLed[0] |= 0x04)
#define vSetP9_3()          (u8aLed[0] |= 0x10)
#define vSetP9_4()          (u8aLed[0] |= 0x40)
#define vSetP9_5()          (u8aLed[1] |= 0x01)
#define vSetP9_6()          (u8aLed[1] |= 0x04)
#define vSetP9_7()          (u8aLed[1] |= 0x10)
#define vSetP9_8()          (u8aLed[1] |= 0x40)
#endif
#define vSetP10_2()         (PORTBbits.RB15 = 1)
#define vSetP10_3()         (PORTCbits.RC9 = 1)
#define vSetP15_2()         (PORTBbits.RB10 = 1)
#define vSetP17_2()         (PORTBbits.RB13 = 1)
#define vSetP18_1()         (PORTCbits.RC0 = 1)
#define vSetP18_2()         (PORTCbits.RC1 = 1)
#define vSetP18_3()         (PORTCbits.RC2 = 1)
#define vSetP18_4()         (PORTAbits.RA9 = 1)
#define vSetP18_5()         (PORTBbits.RB7 = 1)
#define vSetP18_7()         (PORTAbits.RA4 = 1)
#define vSetP18_8()         (PORTBbits.RB8 = 1)
#define vSetReset()         (PORTCbits.RC7 = 1)

/* Application HW Interfaces */
#define bIsFuncKeySw1Set()  (bIsSetP1_1())
#define bIsLs1Set()         (bIsSetP1_2())
#define bIsLs2Set()         (bIsSetP1_3())
#define bIsLs3Set()         (bIsSetP1_4())
#define bIsLs4Set()         (bIsSetP1_5())
#define bIsNsfNoSet()       (bIsSetP1_6())
#define bIsOsfNoSet()       (bIsSetP1_7())
#define bIsNsrNoSet()       (bIsSetP1_8())
#define bIsOsrNoSet()       (bIsSetP1_9())
#define bIsBl1NoSet()       (bIsSetP1_10())

#define vClrCageSolenoid()  (vClrP2_1())
#define vClrBoomSolenoid()  (vClrP2_2())
#define vClrEmergencyPump() (vClrP2_3())
#define vClrPto()           (vClrP2_4())
#define vClrLegSolenoid()   (vClrP2_5())
#define vClrBeacons1()      (vClrP2_6())
#define vSetCageSolenoid()  (vSetP2_1())
#define vSetBoomSolenoid()  (vSetP2_2())
#define vSetEmergencyPump() (vSetP2_3())
#define vSetPto()           (vSetP2_4())
#define vSetLegSolenoid()   (vSetP2_5())
#define vSetBeacons1()      (vSetP2_6())

#ifdef CPL2_BUILD
#define bIsTl1NoSet()       (bIsSetP3_5())
#define bIsBeaconSet()      (bIsSetP3_6())
#define bIsEStopCageNoSet() (bIsSetP3_7())
#define bIsEStopCageNcSet() (bIsSetP3_8())
#define bIsTl1NcSet()       (bIsSetP3_9())
#define bIsHBrakeSet()      (bIsSetP3_10())
#define bIsPtoSet()         (bIsSetP3_11())
#define bIsHornSet()        (bIsSetP3_12())
#define bIsOsrNcSet()       (bIsSetP3_13())
#define bIsBl1NcSet()       (bIsSetP3_14())
#define bIsNsfNcSet()       (bIsSetP3_15())
#define bIsOsfNcSet()       (bIsSetP3_16())
#define bIsNsrNcSet()       (bIsSetP3_17())
#define bIsWorkLightSet()   (bIsSetP3_18())
#define bIsSpotLightSet()   (bIsSetP3_19())
#else
#define bIsTl1NoSet()       (bIsSetP3_1())
#define bIsPtoSet()         (bIsSetP3_2())
#define bIsHornSet()        (bIsSetP3_3())
#define bIsBl1NcSet()       (bIsSetP3_4())
#define bIsTl1NcSet()       (bIsSetP3_5())
#define bIsBeaconSet()      (bIsSetP3_6())
#define bIsEStopCageNoSet() (bIsSetP3_7())
#define bIsEStopCageNcSet() (bIsSetP3_8())
#define bIsOsrNcSet()       (bIsSetP3_9())
#define bIsHBrakeSet()      (bIsSetP3_10())
#define bIsNsfNcSet()       (bIsSetP3_11())
#define bIsOsfNcSet()       (bIsSetP3_12())
#endif

#define vClrLegsStowedLed() (vClrP4_1())
#define vClrNsrLed()        (vClrP4_2())
#define vClrNsfLed()        (vClrP4_3())
#define vClrUnStowedLed()   (vClrP4_4())
#define vClrOsrLed()        (vClrP4_5())
#define vClrOsfLed()        (vClrP4_6())
#define vClrEngStartLed()   (vClrP4_7())
#define vClrEngStopLed()    (vClrP4_8())
#define vSetLegsStowedLed() (vSetP4_1())
#define vSetNsrLed()        (vSetP4_2())
#define vSetNsfLed()        (vSetP4_3())
#define vSetUnStowedLed()   (vSetP4_4())
#define vSetOsrLed()        (vSetP4_5())
#define vSetOsfLed()        (vSetP4_6())
#define vSetEngStartLed()   (vSetP4_7())
#define vSetEngStopLed()    (vSetP4_8())

#define bIsEngStartSet()    (bIsSetP7_1())
#define bIsEngStopSet()     (bIsSetP7_2())
#define bIsEPumpSet()       (bIsSetP7_3())
#ifndef CPL2_BUILD
#define bIsNsrNcSet()       (bIsSetP7_4())
#endif
#define bIsEStopNcSet()     (bIsSetP7_5())
#define bIsEStopNoSet()     (bIsSetP7_6())
#define bIsFuncKeySw3Set()  (bIsSetP7_7())
#define bIsFuncKeySw2Set()  (bIsSetP7_8())

#define vClrLegsWarnLed()   (vClrP9_1())
#define vClrLegsCageLed()   (vClrP9_2())
#define vClrLegsCabLed()    (vClrP9_3())
#define vClrPtoLed()        (vClrP9_4())
#define vClrCagePtoLed()    (vClrP9_5())
#define vSetLegsWarnLed()   (vSetP9_1())
#define vSetLegsCageLed()   (vSetP9_2())
#define vSetLegsCabLed()    (vSetP9_3())
#define vSetPtoLed()        (vSetP9_4())
#define vSetCagePtoLed()    (vSetP9_5())

#define vClrHBrakeBuzzer()  (vClrP15_2())
#define vSetHBrakeBuzzer()  (vSetP15_2())

#define vClrCageBuzzer()    (vClrP17_2())
#define vSetCageBuzzer()    (vSetP17_2())

#ifdef CPL2_BUILD
#define vClrWorkLight()     (vClrP18_1())
#define vClrSpotLight()     (vClrP18_2())
/* #define vClrSpare1()        (vClrP18_4()) *9* DELETED */
#define vClrBeacons2()      (vClrP18_3()) /* *9* ADDED */
#define vClrSpare2()        (vClrP18_4())
#define vClrHorn()          (vClrP18_5())
/* #define vClrBeacons()      (vClrP18_7()) *9* DELETED */
#define vClrBeacons3()      (vClrP18_7()) /* *9* ADDED */
#define vClrEngStart()      (vClrP18_8())
#define vSetWorkLight()     (vSetP18_1())
#define vSetSpotLight()     (vSetP18_2())
/* #define vSetSpare1()        (vSetP18_4()) *9* DELETED */
#define vSetBeacons2()      (vSetP18_3()) /* *9* ADDED */
#define vSetSpare2()        (vSetP18_4())
#define vSetHorn()          (vSetP18_5())
/* #define vSetBeacons()      (vSetP18_7()) *9* DELETED */
#define vSetBeacons3()      (vSetP18_7()) /* *9* ADDED */
#define vSetEngStart()      (vSetP18_8())
#else
#define vClrSpare1()        (vClrP18_1())
#define vClrSpare2()        (vClrP18_2())
#define vClrBeacons2()      (vClrP18_3())
#define vClrSpare3()        (vClrP18_4())
#define vClrHorn()          (vClrP18_5())
#define vClrBeacons3()      (vClrP18_7())
#define vClrEngStart()      (vClrP18_8())
#define vSetSpare1()        (vSetP18_1())
#define vSetSpare2()        (vSetP18_2())
#define vSetBeacons2()      (vSetP18_3())
#define vSetSpare3()        (vSetP18_4())
#define vSetHorn()          (vSetP18_5())
#define vSetBeacons3()      (vSetP18_7())
#define vSetEngStart()      (vSetP18_8())
#endif
/********************************************************************************* Local TypeDefs */
/*********************************************************************** Local Variables (static) */
//static uint8_t u8aPwm[16];
static uint8_t u8aLed[4];
//static uint8_t u8aTest[16];
static uint8_t u8aU16Gpio[2];
#ifdef CPL2_BUILD
static uint8_t u8aU19Gpio[2];
#endif
static uint8_t u8aU2Gpio[2];
static uint8_t u8aU4Gpio[2];
static uint8_t u8EngRunTmr;
static uint8_t u8EnState;
static uint8_t u8Flags;
static uint8_t u8Flags2;
static uint8_t u8OpState;
static uint8_t u8RunState;
static uint8_t u8Tick;
static uint8_t u8TickH;

/********************************************************************** Local Function Prototypes */

/*********************************************************************** Local Constants (static) */
/* Configuration Settings */
#pragma config ICS = PGD3
#pragma config JTAGEN = OFF
#pragma config ALTI2C1 = OFF
#pragma config ALTI2C2 = OFF
#pragma config WDTWIN = WIN75
#pragma config WDTPOST = PS32768
#pragma config WDTPRE = PR128
#pragma config PLLKEN = ON
#pragma config WINDIS = OFF
#pragma config FWDTEN = OFF
#pragma config POSCMD = XT
#pragma config OSCIOFNC = ON
#pragma config IOL1WAY = ON
#pragma config FCKSM = CSECMD
#pragma config FNOSC = FRC
#pragma config IESO = OFF
#pragma config GWRP = OFF
#pragma config GCP = OFF

static const uint8_t u8aLedInit[TLC59116_NUM_REG] = {
    LED_MODE_NORMAL, /* MODE1 */
    LED_MODE_ERREN | LED_MODE_DIM | LED_MODE_STPCHG, /* MODE2 */
    LED_PWM_OFF, LED_PWM_OFF, LED_PWM_OFF, LED_PWM_OFF, LED_PWM_OFF, LED_PWM_OFF, /* PWM */
    LED_PWM_OFF, LED_PWM_OFF, LED_PWM_OFF, LED_PWM_OFF, LED_PWM_OFF, LED_PWM_OFF, /* PWM */
    LED_PWM_OFF, LED_PWM_OFF, LED_PWM_OFF, LED_PWM_OFF, /* PWM */
    LED_MAX_BRIGHT, /* GRPPWM */
    BLINK_FREQ, /* GRPFREQ */
    LED_GRPPWM, /* LEDOUT1 */
    LED_GRPPWM, /* LEDOUT2 */
    LED_GRPPWM, /* LEDOUT3 */
    LED_GRPPWM /* LEDOUT4 */
};
static const uint8_t u8aMcp23017U16Init[MCP23017_NUM_REG] = {
    0x3F, /* GPA7, GPA6 ddr = outputs, GPA5 - GPA0 ddr = inputs */
    0xFF, /* GPB7 - GPB0 ddr = inputs */
    0x00, /* GPA7 - GPA0 normal polarity */
    0x00, /* GPB7 - GPB0 normal polarity */
    0x00, /* GPA7 - GPA0 interrupts diabled */
    0x00, /* GPB7 - GPB0 interrupts diabled */
    0x00, /* GPA7 - GPA0 defval =0 */
    0x00, /* GPB7 - GPB0 defval =0 */
    0x00, /* GPA7 - GPA0 intcona =0 */
    0x00, /* GPB7 - GPB0 intcona =0 */
    0x08, /* IOCON */
    0x08, /* IOCON */
    0x00, /* GPA7 - GPA0 pullups disabled */
    0x00, /* GPB7 - GPB0 pullups disabled */
    0x00, /* GPA7 - GPA0 interrupt */
    0x00, /* GPB7 - GPB0 interrupt */
    0x00, /* GPA7 - GPA0 interrupt capture */
    0x00, /* GPB7 - GPB0 interrupt capture */
    0x00, /* GPA7 - GPA0 gpio */
    0x00, /* GPB7 - GPB0 gpio */
    0x00, /* GPA7 - GPA0 olat */
    0x00, /* GPB7 - GPB0 olat */
};
#ifdef CPL2_BUILD
static const uint8_t u8aMcp23017U19Init[MCP23017_NUM_REG] = {
    0x00, /* GPA7 - GPA0 ddr = outputs */
    0xFF, /* GPB7 - GPB0 ddr = inputs */
    0x00, /* GPA7 - GPA0 normal polarity */
    0x00, /* GPB7 - GPB0 normal polarity */
    0x00, /* GPA7 - GPA0 interrupts diabled */
    0x00, /* GPB7 - GPB0 interrupts diabled */
    0x00, /* GPA7 - GPA0 defval =0 */
    0x00, /* GPB7 - GPB0 defval =0 */
    0x00, /* GPA7 - GPA0 intcona =0 */
    0x00, /* GPB7 - GPB0 intcona =0 */
    0x08, /* IOCON */
    0x08, /* IOCON */
    0x00, /* GPA7 - GPA0 pullups disabled */
    0x00, /* GPB7 - GPB0 pullups disabled */
    0x00, /* GPA7 - GPA0 interrupt */
    0x00, /* GPB7 - GPB0 interrupt */
    0x00, /* GPA7 - GPA0 interrupt capture */
    0x00, /* GPB7 - GPB0 interrupt capture */
    0x00, /* GPA7 - GPA0 gpio */
    0x00, /* GPB7 - GPB0 gpio */
    0x00, /* GPA7 - GPA0 olat */
    0x00, /* GPB7 - GPB0 olat */
};
#endif
static const uint8_t u8aMcp23017U2Init[MCP23017_NUM_REG] = {
    0x00, /* GPA7 - GPA0 ddr = outputs */
    0x3F, /* GPB7, GPB6 ddr = outputs, GPB5 - GPB0 ddr = inputs */
    0x00, /* GPA7 - GPA0 normal polarity */
    0x00, /* GPB7 - GPB0 normal polarity */
    0x00, /* GPA7 - GPA0 interrupts diabled */
    0x00, /* GPB7 - GPB0 interrupts diabled */
    0x00, /* GPA7 - GPA0 defval =0 */
    0x00, /* GPB7 - GPB0 defval =0 */
    0x00, /* GPA7 - GPA0 intcona =0 */
    0x00, /* GPB7 - GPB0 intcona =0 */
    0x08, /* IOCON */
    0x08, /* IOCON  */
    0x00, /* GPA7 - GPA0 pullups disabled */
    0x00, /* GPB7 - GPB0 pullups disabled */
    0x00, /* GPA7 - GPA0 interrupt */
    0x00, /* GPB7 - GPB0 interrupt */
    0x00, /* GPA7 - GPA0 interrupt capture */
    0x00, /* GPB7 - GPB0 interrupt capture */
    0x00, /* GPA7 - GPA0 gpio */
    0x00, /* GPB7 - GPB0 gpio */
    0x00, /* GPA7 - GPA0 olat */
    0x00, /* GPB7 - GPB0 olat */
};
static const uint8_t u8aMcp23017U4Init[MCP23017_NUM_REG] = {
    0xFF, /* GPA7 - GPA0 ddr =inputs */
    0xFF, /* GPB7 - GPB0 ddr =inputs */
    0x00, /* GPA7 - GPA0 normal polarity */
    0x00, /* GPB7 - GPB0 normal polarity */
    0x00, /* GPA7 - GPA0 interrupts diabled */
    0x00, /* GPB7 - GPB0 interrupts diabled */
    0x00, /* GPA7 - GPA0 defval =0 */
    0x00, /* GPB7 - GPB0 defval =0 */
    0x00, /* GPA7 - GPA0 intcona =0 */
    0x00, /* GPB7 - GPB0 intcona =0 */
    0x08, /* IOCON */
    0x08, /* IOCON */
    0x00, /* GPA7 - GPA0 pullups disabled */
    0x00, /* GPB7 - GPB0 pullups disabled */
    0x00, /* GPA7 - GPA0 interrupt */
    0x00, /* GPB7 - GPB0 interrupt */
    0x00, /* GPA7 - GPA0 interrupt capture */
    0x00, /* GPB7 - GPB0 interrupt capture */
    0x00, /* GPA7 - GPA0 gpio */
    0x00, /* GPB7 - GPB0 gpio */
    0x00, /* GPA7 - GPA0 olat */
    0x00, /* GPB7 - GPB0 olat */
};

/*************************************************************************** Function Definitions */
static void vDoControl(void);
static void vRtcInit(void);

/********************************************************************************** _T1Interrupt ***
 * Name:        Timer1 ISR Handler
 * Parameters:  None
 * Returns:     None
 * Description: Occurs every 10msec
 **************************************************************************************************/
void __attribute__((interrupt(no_auto_psv))) _CNInterrupt(void) {   /* *11 * ADDED */
    IFS1bits.CNIF = 0;                                              /* *11 * ADDED */
    u8EngRunTmr = ENG_NOT_RUNNING_DELAY;                            /* *11 * ADDED */
    vSetEngRunning();                                               /* *11 * ADDED */
}                                                                   /* *11 * ADDED */

/********************************************************************************** _T1Interrupt ***
 * Name:        Timer1 ISR Handler
 * Parameters:  None
 * Returns:     None
 * Description: Occurs every 10msec
 **************************************************************************************************/
void __attribute__((interrupt(no_auto_psv))) _T1Interrupt(void) {
    IFS0bits.T1IF = 0;
    u8Tick++;
    if (u8Tick == 100) {
        u8Tick = 0;
    }
}

/****************************************************************************************** main ***
 * Name:        Main Routine
 * Parameters:  None
 * Returns:     None
 * Description: Performs the following:
 *              Initialises PLL to generate an Fosc = 32MHz, Fcy = 16MHz
 **************************************************************************************************/
int main(void) {
    /* Clear reset indications */
    RCON = 0;

    /* Configure PLL for 32Mhz */
    PLLFBD = FOSC - 2;
    CLKDIVbits.PLLPOST = 1;
    CLKDIVbits.PLLPRE = 0;
    __builtin_write_OSCCONH(0x03);
    __builtin_write_OSCCONL(OSCCON | 0x01);

    /* Wait for Clock switch to occur */
    while (OSCCONbits.COSC != 0b011) {
    }

    /* Wait for PLL to lock */
    while (OSCCONbits.LOCK != 1) {
    }

    /* AN0 only, the KL15 input is analog */
    ANSELA = 1;
    ANSELB = 0;
    ANSELC = 0;
    AD1CON1bits.SSRC = 7;
    AD1CON1bits.ASAM = 1;
    AD1CON3bits.SAMC = 5;
    AD1CON3bits.ADCS = 31;
    AD1CON1bits.ADON = 1;

    /* Set up remappable inputs */
    RPINR26 = 0x0025; // C1RX

    /* Set up remappable outputs */
    RPOR2 = 0x000E; // C1TX

    /* Initialise Digital outputs */
    PORTA = 0;
    TRISA = 0xF96D; /* RA1, RA4, RA7, RA9, RA10*/
    PORTB &= 0;
    TRISB = 0x007B; /* RB2, RB7 - RB15 */
    PORTC = 0;
    TRISC = 0x0130; /* RC0 - RC3, RC6 - RC7,  RC9 - RC15*/
    /* CNENCbits.CNIEC8 = 1; *11* DELETED */

    /* Enable peripherals */
    PMD1 = 0xF77E; /* TMR1, I2C1, ADC enabled */
    PMD2 = 0x0F0F;
    PMD3 = 0x0482;
    PMD4 = 0x000C;
    PMD6 = 0x0700;
    PMD7 = 0x0018;

    /* Initialise real time clock */
    vRtcInit();
    i2c_vInit(I2C_BRG);
    u8OpState = OP_STATE_INIT;

    /* Initialise ALT_W Interface */
    CNENCbits.CNIEC8 = 1;   /* *11 * ADDED */
    IFS1bits.CNIF = 0;      /* *11 * ADDED */
    IPC4bits.CNIP = 5;      /* *11 * ADDED */
    IEC1bits.CNIE = 1;      /* *11 * ADDED */
    
    /* Run application */
    for (;;) {
        switch (u8OpState) {
            case OP_STATE_INIT:
                if (u8Tick != u8TickH) {
                    vClrReset();
                    u8OpState = OP_STATE_ENBL;
                    u8TickH = u8Tick;
                }
                break;
            case OP_STATE_ENBL:
                switch (u8EnState) {
                    case ENBL_STATE_IDLE:
                        if (u8Tick != u8TickH) {
                            vSetReset();
                            u8EnState = ENBL_STATE_LED_ENABLE;
                        }
                        break;
                    case ENBL_STATE_LED_ENABLE:
                        i2c_vWrite(TLC59116_ADDRESS, LED_AUTO_INC_1 | LED_MODE1_REG,
                                u8aLedInit, TLC59116_NUM_REG);
                        u8EnState = ENBL_STATE_U2_ENABLE;
                        break;
                    case ENBL_STATE_U2_ENABLE:
                        if ((i2c_u8GetStatus() == I2C_STATUS_TXFER_COMPLETE) ||
                                (i2c_u8GetStatus() == I2C_STATUS_ACK_FLT)) {
                            i2c_vWrite(MCP23017U2_ADDRESS, MCP23017_IODIRA_REG,
                                    u8aMcp23017U2Init, MCP23017_NUM_REG);
                            u8EnState = ENBL_STATE_U4_ENABLE;
                        }
                        break;
                    case ENBL_STATE_U4_ENABLE:
                        if ((i2c_u8GetStatus() == I2C_STATUS_TXFER_COMPLETE) ||
                                (i2c_u8GetStatus() == I2C_STATUS_ACK_FLT)) {
                            i2c_vWrite(MCP23017U4_ADDRESS, MCP23017_IODIRA_REG,
                                    u8aMcp23017U4Init, MCP23017_NUM_REG);
                            u8EnState = ENBL_STATE_U16_ENABLE;
                        }
                        break;
#ifdef CPL2_BUILD
                    case ENBL_STATE_U16_ENABLE:
                        if ((i2c_u8GetStatus() == I2C_STATUS_TXFER_COMPLETE) ||
                                (i2c_u8GetStatus() == I2C_STATUS_ACK_FLT)) {
                            i2c_vWrite(MCP23017U16_ADDRESS, MCP23017_IODIRA_REG,
                                    u8aMcp23017U16Init, MCP23017_NUM_REG);
                            u8EnState = ENBL_STATE_U19_ENABLE;
                        }
                        break;

                    case ENBL_STATE_U19_ENABLE:
                        if ((i2c_u8GetStatus() == I2C_STATUS_TXFER_COMPLETE) ||
                                (i2c_u8GetStatus() == I2C_STATUS_ACK_FLT)) {
                            i2c_vWrite(MCP23017U19_ADDRESS, MCP23017_IODIRA_REG,
                                    u8aMcp23017U19Init, MCP23017_NUM_REG);
                            u8EnState = ENBL_STATE_WAIT;
                        }
                        break;
#else
                    case ENBL_STATE_U16_ENABLE:
                        if ((i2c_u8GetStatus() == I2C_STATUS_TXFER_COMPLETE) ||
                                (i2c_u8GetStatus() == I2C_STATUS_ACK_FLT)) {
                            i2c_vWrite(MCP23017U16_ADDRESS, MCP23017_IODIRA_REG,
                                    u8aMcp23017U16Init, MCP23017_NUM_REG);
                            u8EnState = ENBL_STATE_WAIT;
                        }
                        break;
#endif
                    case ENBL_STATE_WAIT:
                        if ((i2c_u8GetStatus() == I2C_STATUS_TXFER_COMPLETE) ||
                                (i2c_u8GetStatus() == I2C_STATUS_ACK_FLT)) {
                            u8OpState = OP_STATE_RUNNING;
                            u8RunState = RUN_STATE_IDLE;
                            u8TickH = u8Tick;
                        }
                        break;
                    default:
                        u8EnState = ENBL_STATE_IDLE;
                        break;
                }
                break;
            case OP_STATE_RUNNING:
                switch (u8RunState) {
                    case RUN_STATE_IDLE:
                        if (u8Tick != u8TickH) {
                            i2c_vRead(MCP23017U2_ADDRESS, MCP23017_GPIOB_REG, 1);
                            u8RunState = RUN_STATE_GET_U2_IP;
                        }
                        break;
                    case RUN_STATE_GET_U2_IP:
                        if (i2c_u8GetStatus() == I2C_STATUS_TXFER_COMPLETE) {
                            i2c_vGetData(u8aU2Gpio + 1, 1);
                            i2c_vRead(MCP23017U4_ADDRESS, MCP23017_GPIOA_REG, 2);
                            u8RunState = RUN_STATE_GET_U4_IP;
                        }
                        break;
                    case RUN_STATE_GET_U4_IP:
                        if (i2c_u8GetStatus() == I2C_STATUS_TXFER_COMPLETE) {
                            i2c_vGetData(u8aU4Gpio, 2);
                            i2c_vRead(MCP23017U16_ADDRESS, MCP23017_GPIOA_REG, 2);
                            u8RunState = RUN_STATE_GET_U16_IP;
                        }
                        break;
#ifdef CPL2_BUILD
                    case RUN_STATE_GET_U16_IP:
                        if (i2c_u8GetStatus() == I2C_STATUS_TXFER_COMPLETE) {
                            i2c_vGetData(u8aU16Gpio, 2);
                            /* *8* ADDED */
                            i2c_vRead(MCP23017U19_ADDRESS, MCP23017_GPIOB_REG, 1);
                            /* *8* */
                            u8RunState = RUN_STATE_GET_U19_IP;
                        }
                        break;
                    case RUN_STATE_GET_U19_IP:
                        if (i2c_u8GetStatus() == I2C_STATUS_TXFER_COMPLETE) {
                            i2c_vGetData(u8aU19Gpio + 1, 1);
                            u8RunState = RUN_STATE_PROCESS;
                        }
                        break;
#else
                    case RUN_STATE_GET_U16_IP:
                        if (i2c_u8GetStatus() == I2C_STATUS_TXFER_COMPLETE) {
                            i2c_vGetData(u8aU16Gpio, 2);
                            u8RunState = RUN_STATE_PROCESS;
                        }
                        break;
#endif
                    case RUN_STATE_PROCESS:
                        if (bIsKL15Set()) {
                            if (ADC1BUF0 < KL15_OFF) {
                                vClrKL15();
                            }
                        } else {
                            if (ADC1BUF0 > KL15_ON) {
                                vSetKL15();
                            }
                        }
                        vDoControl();
                        u8RunState = RUN_STATE_PUT_U2_OP;
                        break;
                    case RUN_STATE_PUT_U2_OP:
                        i2c_vWrite(MCP23017U2_ADDRESS, MCP23017_GPIOA_REG,
                                u8aU2Gpio, 1);
                        u8RunState = RUN_STATE_PUT_LED_OP;
                        break;
                    case RUN_STATE_PUT_LED_OP:
                        if (i2c_u8GetStatus() == I2C_STATUS_TXFER_COMPLETE) {
                            i2c_vWrite(TLC59116_ADDRESS, LED_AUTO_INC_1 | LED_LEDOUT0_REG, u8aLed, 4);
                            u8RunState = RUN_STATE_WAIT;
                        }
                        break;
                    case RUN_STATE_WAIT:
                        if (i2c_u8GetStatus() == I2C_STATUS_TXFER_COMPLETE) {
                            u8TickH = u8Tick;
                            u8RunState = RUN_STATE_IDLE;
                        }
                        break;
                    default:
                        u8TickH = u8Tick;
                        u8RunState = RUN_STATE_IDLE;
                        break;
                }
                break;
            default:
                u8OpState = OP_STATE_INIT;
                break;
        }
    }

    return 0;
}

/************************************************************************************ vDoControl ***
 * Name:        Do Control
 * Parameters:  None
 * Returns:     None
 * Description: 
 **************************************************************************************************/
static void vDoControl(void) {

    if (!bIsLs1Set() && !bIsLs2Set() && !bIsLs3Set() && !bIsLs4Set()){
        vSetLegsUnstowed();
    } else {
        vClrLegsUnstowed();
    }
    
    if (!bIsEStopCageNcSet() || bIsEStopCageNoSet() || !bIsEStopNcSet() || bIsEStopNoSet()) {
        vSetEStop();
    } else {
        vClrEStop();
    }    
    if (bIsLegsUnstowed() &&
            !bIsNsfNoSet() && !bIsOsfNoSet() && !bIsNsrNoSet() && !bIsOsrNoSet() &&
            bIsNsfNcSet() && bIsOsfNcSet() && bIsNsrNcSet() && bIsOsrNcSet()) {
        vSetLegsDown();     /* *5* CHANGED */
    } else {
        vClrLegsDown();     /* *5* CHANGED */
    }
    
    if (bIsTl1NoSet() && !bIsTl1NcSet() &&          /* *5* ADDED */
            bIsBl1NoSet() && !bIsBl1NcSet()) {      /* *5* ADDED */
        vSetBoomHome();                             /* *5* ADDED */
    } else {                                        /* *5* ADDED */
        vClrBoomHome();                             /* *5* ADDED */
    }                                               /* *5* ADDED */

    if (bAreLegsDown()) {                           /* *5* ADDED */
        vSetLegsDownLatched();                      /* *5* ADDED */
    } else if (!bAreLegsDown() && bIsBoomHome()) {  /* *5* ADDED */
        vClrLegsDownLatched();                      /* *5* ADDED */
    }                                               /* *5* ADDED */
    
    if (bIsKL15Set() && bIsPtoSet() && !bIsHBrakeSet() && !bIsEStopSet()) {
        vSetPto();
        vSetPtoLed();
        vSetCagePtoLed();
        vSetHourPwr();
        vSetHourEn();
        vSetSysEnable();
    } else {
        vClrPto();
        vClrPtoLed();
        vClrCagePtoLed();
        vClrHourPwr();
        vClrHourEn();
        vClrSysEnable();
    }
 
    if (bIsSysEnabled() && /* *5* Changed to SysEnabled */
            bIsEPumpSet()) {
        vSetEmergencyPump();
    } else {
        vClrEmergencyPump();
    }
        
    if (bIsSysEnabled() && (bIsFuncKeySw1Set() || bIsFuncKeySw2Set()) && /* *5* Changed to SysEnabled */
            bIsLegsDownLatched()) {
        vSetBoomSolenoid();
    } else {
        vClrBoomSolenoid();
    }
    
    if (bIsSysEnabled() && bIsFuncKeySw2Set() && /* *5* Changed to SysEnabled */
            bIsLegsDownLatched()) {
        vSetCageSolenoid();
    } else {
        vClrCageSolenoid();
    }

    if (bIsSysEnabled() && /* *5* Changed to SysEnabled */
            (bIsBoomHome() ||
            !bIsLegsDownLatched())&&
            bIsFuncKeySw3Set()) {
        vSetLegSolenoid();
    } else {
        vClrLegSolenoid();
    }

    if ((bIsKL15Set() && bIsPtoSet() &&
             (bIsEngStopSet() || bIsEPumpSet() || bIsEStopSet()))) {
        vSetEngStop();
    } else {
        vClrEngStop();
    }    

    if (bIsKL15Set() && bIsHBrakeSet() &&
            (bIsPtoSet() || (!bIsLs1Set() || !bIsLs2Set() || !bIsLs3Set() || !bIsLs4Set()))) {
        vSetHBrakeBuzzer();
    } else {
        vClrHBrakeBuzzer();
    }

    if (bIsKL15Set() && bIsPtoSet() && 
            (bIsEStopSet() ||
            (!bIsBoomHome() &&
            (bIsNsfNoSet() || !bIsNsfNcSet() || bIsOsfNoSet() || !bIsOsfNcSet())))) {
        vSetCageBuzzer();
    } else {
        vClrCageBuzzer();
    }

    if (bIsKL15Set() &&
            !bIsBoomHome() &&
            (bIsNsfNoSet() || !bIsNsfNcSet() || bIsOsfNoSet() || !bIsOsfNcSet())) {
        vSetLegsCageLed();
        /* vSetLegsStowedLed(); *10 *DELETED */ 
        vSetLegsWarnLed(); /* *10* ADDED */
    } else {
        vClrLegsCageLed();
        /* vClrLegsStowedLed(); *10* DELETED */
        vClrLegsWarnLed(); /* *10* ADDED */
    }

    if (bIsKL15Set() && bIsLs1Set() && bIsLs2Set() && bIsLs3Set() && bIsLs4Set()){ /* *10* ADDED */
        vSetLegsStowedLed();                                                       /* *10* ADDED */
    } else {                                                                       /* *10* ADDED */
        vClrLegsStowedLed();                                                       /* *10* ADDED */
    }                                                                              /* *10* ADDED */
        
#ifdef CPL2_BUILD
    if (bIsKL15Set()) {
        /* vSetSpare1(); *9* DELETED */
        vSetSpare2();
    } else {
        /* vClrSpare1(); *9* DELETED */
        vClrSpare2();
    }
    
    if (bIsBeaconSet()) {
        vSetBeacons1(); 
        vSetBeacons2(); /* ADDED */
        vSetBeacons3(); /* ADDED */
    } else {
        vClrBeacons1();
        vClrBeacons2(); /* ADDED */
        vClrBeacons3(); /* ADDED */
    }
    
    /* if (bIsWorkLightSet()) { *9* DELETED */
    if (bIsKL15Set() && bIsWorkLightSet()) { /* ADDED */
        vSetWorkLight();
    } else {
        vClrWorkLight();
    }
    
    if (bIsSpotLightSet()) {
        vSetSpotLight();
    } else {
        vClrSpotLight();
    }
#else
    if (bIsKL15Set()) {
        vSetSpare1();
        vSetSpare2();
        vSetSpare3();
    } else {
        vClrSpare1();
        vClrSpare2();
        vClrSpare3();
    }
    
    if (bIsBeaconSet()) {
        vSetBeacons1();
        vSetBeacons2();
        vSetBeacons3();
    } else {
        vClrBeacons1();
        vClrBeacons2();
        vClrBeacons3();
    }
#endif    
    if (bIsKL15Set() && !bIsNsfNoSet() && bIsNsfNcSet()) {
        vSetNsfLed();
    } else {
        vClrNsfLed();
    }

    if (bIsKL15Set() && !bIsOsfNoSet() && bIsOsfNcSet()) {
        vSetOsfLed();
    } else {
        vClrOsfLed();
    }

    if (bIsKL15Set() && !bIsNsrNoSet() && bIsNsrNcSet()) {
        vSetNsrLed();
    } else {
        vClrNsrLed();
    }

    if (bIsKL15Set() && !bIsOsrNoSet() && bIsOsrNcSet()) {
        vSetOsrLed();
    } else {
        vClrOsrLed();
    }

    if (bIsKL15Set() &&
            (!bIsLs1Set() || !bIsLs2Set() || !bIsLs3Set() || !bIsLs4Set())) {
        vSetUnStowedLed();
        vSetLegsCabLed();
    } else {
        vClrUnStowedLed();
        vClrLegsCabLed();
    }

    IEC1bits.CNIE = 0;      /* *11* CHANGED */
    if (u8EngRunTmr == 0) { /* *11* CHANGED */
        vClrEngRunning();   /* *11* CHANGED */
    } else {                /* *11* CHANGED */
        u8EngRunTmr--;      /* *11* CHANGED */
    }                       /* *11* CHANGED */
    IEC1bits.CNIE = 1;      /* *11* CHANGED */

    if (bIsKL15Set() && bIsPtoSet() && bIsEngStartSet() && /* *5* Added Pto */
            !bIsEngRunning()) { /* *7* Added eng not running */
        vSetEngStart();
    } else {
        vClrEngStart();
    }
    
    if (bIsKL15Set() && bIsPtoSet()) { /* *5* Added Pto */
        vSetEngStartLed();
    } else {
        vClrEngStartLed();
    }

    if (bIsKL15Set() && bIsPtoSet()) { /* *5* Added Pto */
        vSetEngStopLed();
    } else {
        vClrEngStopLed();
    }

   if (bIsKL15Set() && bIsPtoSet() && bIsHornSet()) {
        vSetHorn();
    } else {
        vClrHorn();
    }
}

/************************************************************************************** vRtcInit ***
 * Name:        Initialise Timer1 to generate an interrupt every 1msec
 * Parameters:  None
 * Returns:     None
 * Description: Initialises Timer1
 **************************************************************************************************/
static void vRtcInit(void) {
    PR1 = RTC_PERIOD;
    T1CONbits.TCKPS = RTC_SCALE;
    T1CONbits.TON = 1;
    IFS0bits.T1IF = 0;
    IPC0bits.T1IP = 5;
    IEC0bits.T1IE = 1;
}

/*
 * End of Workfile: main.c
 */


