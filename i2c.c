/***************************************************************************************************
 * Name:    I2C Driver
 * File:    i2c.c
 * Author:  RDTek
 * Date:    15/11/15
 *
 * Copyright:
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 15/11/15 | RDTek | Created.
 *
 * Description:
 * Simple I2C Master
 * I2C module gets a clock of Fp/2
 **************************************************************************************************/

/**************************************************************************************** Include */
#include "i2c.h"

/******************************************************************************* Global Variables */

/*********************************************************************** Local Macros and Defines */
#define I2C_BUF_SIZE    32
#define I2C_READ    0x01
#define I2C_WRITE   0x00
#define STATE_START     1
#define STATE_SLV_ADD   2
#define STATE_SLV_REG   3
#define STATE_RESTART   4
#define STATE_READ_ST   5
#define STATE_READ      6
#define STATE_READ_ACK  7
#define STATE_WRITE     8
#define STATE_TFER_DONE 9
#define STATE_ACK_FLT   10
#define STATE_STOP      0

/********************************************************************************* Local TypeDefs */

/*********************************************************************** Local Constants (static) */

/*********************************************************************** Local Variables (static) */
static uint8_t u8Read;
static uint8_t u8aBuf[I2C_BUF_SIZE];
static uint8_t *pu8Buf;
static uint8_t u8I2CStatus;
static uint8_t u8NOfB;
static uint8_t u8SlvAdd;
static uint8_t u8SlvReg;
static uint8_t u8State;

/********************************************************************** Local Function Prototypes */
static void vCopyN(const uint8_t *src, uint8_t *dest, uint8_t num);

/****************************************************************************** _MI2C1_Interrupt ***
 * Name:        Master I2C1 ISR Handler
 * Parameters:  None
 * Returns:     None
 * Description: 
 **************************************************************************************************/
void __attribute__((interrupt(no_auto_psv))) _MI2C1Interrupt(void) {
    IFS1bits.MI2C1IF = 0;

    (void)I2C1CON;
    (void)I2C1STAT;
    switch(u8State) {
        case STATE_START:
            I2C1STATbits.S = 0;
            I2C1TRN = u8SlvAdd |I2C_WRITE;
            u8State = STATE_SLV_ADD;
            break;
        case STATE_SLV_ADD:
            if (I2C1STATbits.ACKSTAT) {
                I2C1CONbits.PEN = 1;
                u8State = STATE_ACK_FLT;
            } else {
                I2C1TRN = u8SlvReg;
                u8State = STATE_SLV_REG;
            }
            break;
        case STATE_SLV_REG:
            if (I2C1STATbits.ACKSTAT) {
                I2C1CONbits.PEN = 1;
                u8State = STATE_ACK_FLT;
            } else {
                if (u8Read) {
                    I2C1CONbits.RSEN = 1;
                    u8State = STATE_RESTART;
                } else {
                    pu8Buf = u8aBuf;
                    I2C1TRN = *pu8Buf++;
                    u8NOfB--;
                    u8State = STATE_WRITE;
                }
            }
            break;
        case STATE_RESTART:
            I2C1TRN = u8SlvAdd |I2C_READ;
            u8State = STATE_READ_ST;
            break;
        case STATE_READ_ST:
            I2C1CONbits.RCEN = 1;
            pu8Buf = u8aBuf;
            u8State = STATE_READ;
            break;
        case STATE_READ:
            *pu8Buf++ = I2C1RCV;
            u8NOfB--;
            if (u8NOfB) {
                I2C1CONbits.ACKDT = 0;
            } else {
                I2C1CONbits.ACKDT = 1;
            }
            I2C1CONbits.ACKEN = 1;
            u8State = STATE_READ_ACK;
            break;
        case STATE_READ_ACK:
            if (u8NOfB) {
                I2C1CONbits.RCEN = 1;
                u8State = STATE_READ;
            } else {
                I2C1CONbits.PEN = 1;
                u8State = STATE_TFER_DONE;
            }
            break;
        case STATE_WRITE:
            if (I2C1STATbits.ACKSTAT) {
                I2C1CONbits.PEN = 1;
                u8State = STATE_ACK_FLT;
            } else {
                if (u8NOfB) {
                    I2C1TRN = *pu8Buf++;
                    u8NOfB--;
                } else {
                    I2C1CONbits.PEN = 1;
                    u8State = STATE_TFER_DONE;
                }
            }
            break;
        case STATE_TFER_DONE:
            u8I2CStatus = I2C_STATUS_TXFER_COMPLETE;
            u8State = STATE_STOP;
            break;
        case STATE_ACK_FLT:
            u8I2CStatus = I2C_STATUS_ACK_FLT;
           u8State = STATE_STOP;
           break;
        default:
            break;
    }
}

/******************************************************************************** i2c_u8GetStatus***
 * Name:        Get Status
 * Parameters:  None
 * Returns:     u8I2CStatus
 * Description: Reads I2C drive status
 **************************************************************************************************/
uint8_t i2c_u8GetStatus(void) {
    return u8I2CStatus;
}

/************************************************************************************* i2c_vInit ***
 * Name:        Initialise I2c Interface
 * Parameters:  clockDiv
 * Returns:     None
 * Description: Initialises the I2c interface
 **************************************************************************************************/
void i2c_vInit(uint8_t clockDiv) {
    I2C1BRG = clockDiv;
    I2C1CONbits.I2CEN = 1;
    IFS1bits.MI2C1IF = 0;
    IPC4bits.MI2C1IP = 5;
    IEC1bits.MI2C1IE = 1;
    u8I2CStatus = I2C_STATUS_IDLE;
}


/********************************************************************************** i2c_vGetData ***
 * Name:        Get Data
 * Parameters:  buf[]
 *              nOfB
 * Returns:     None
 * Description: Starts an I2C read transfer with a 7bit data address.
 **************************************************************************************************/
void i2c_vGetData(uint8_t buf[], uint8_t nOfB) {
    vCopyN(u8aBuf, buf, nOfB);
}

/************************************************************************************* i2c_vRead ***
 * Name:        Read Data
 * Parameters:  slaveAdd
 *              slaveReg
 *              nOfB
 *              *i2SStatus
 * Returns:     None
 * Description: Starts an I2C read transfer with a 7bit data address.
 **************************************************************************************************/
void i2c_vRead(uint8_t slaveAdd, uint8_t slaveReg, uint8_t nOfB) {
    u8SlvAdd = slaveAdd;
    u8SlvReg = slaveReg;
    u8NOfB = nOfB;
    u8I2CStatus = I2C_STATUS_TFER_IN_PROGRESS;
    u8Read = 1;
    u8State = STATE_START;
    I2C1CONbits.SEN = 1;
}

/************************************************************************************ i2c_vWrite ***
 * Name:        Write Data
 * Parameters:  slaveAdd
 *              slaveReg
 *              buf[]
 *              nOfB
 *              *i2SStatus
 * Returns:     None
 * Description: Starts an I2C read transfer with a 7bit data address.
 **************************************************************************************************/
void i2c_vWrite(uint8_t slaveAdd, uint8_t slaveReg, const uint8_t buf[], uint8_t nOfB) {
    u8SlvAdd = slaveAdd;
    u8SlvReg = slaveReg;
    vCopyN(buf, u8aBuf, nOfB);
    u8NOfB = nOfB;
    u8I2CStatus = I2C_STATUS_TFER_IN_PROGRESS;
    u8Read = 0;
    u8State = STATE_START;
    I2C1CONbits.SEN = 1;
}

/**************************************************************************************** vCopyN ***
 * Name:        Copy N
 * Parameters:  src
 *              dest
 *              n
 * Returns:     None
 * Description: Copies n bytes of data from src to dest
 **************************************************************************************************/
void vCopyN(const uint8_t *src, uint8_t *dest, uint8_t n) {
    while (n-- != 0) {
        *dest++ = *src++;
    }
}

/*
 * End of Workfile: i2c.c
 */
