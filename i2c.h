/***************************************************************************************************
 * Name:    I2C Driver Header
 * File:    i2c.h
 * Author:  RDTek
 * Date:    11/02/17
 *
 * Copyright:
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 11/02/17 | RDTek | Created.
 *
 * Description:
 **************************************************************************************************/

#ifndef I2C_H
#define I2C_H
/**************************************************************************************** Include */
#include <xc.h>
//#include <stdbool.h>
#include <stdint.h>

/**************************************************************************************** Defines */
#define I2C_STATUS_TXFER_COMPLETE   4
#define I2C_STATUS_ACK_FLT          3
#define I2C_STATUS_TFER_IN_PROGRESS 2
#define I2C_STATUS_BUSY             1
#define I2C_STATUS_IDLE             0

/******************************************************************************** Global TypeDefs */

/******************************************************************************* Global Constants */

/******************************************************************************* Global Variables */

/******************************************************************* Callback Function Prototypes */

/******************************************************************************** i2c_u8GetStatus***
 * Name:        Get Status
 * Parameters:  None
 * Returns:     u8I2CStatus
 * Description: Reads I2C drive status
 **************************************************************************************************/
extern uint8_t i2c_u8GetStatus(void);

/************************************************************************************* i2c_vInit ***
 * Name:        Initialise I2c Interface
 * Parameters:  clockDiv
 * Returns:     None
 * Description: Initialises the I2c interface
 **************************************************************************************************/
extern void i2c_vInit(uint8_t clockDiv);

/********************************************************************************** i2c_vGetData ***
 * Name:        Get Data
 * Parameters:  buf[]
 *              nOfB
 * Returns:     None
 * Description: Starts an I2C read transfer with a 7bit data address.
 **************************************************************************************************/
extern void i2c_vGetData(uint8_t buf[], uint8_t nOfB);

/************************************************************************************* i2c_vRead ***
 * Name:        Read Data
 * Parameters:  slaveAdd
 *              slaveReg
 *              nOfB
 *              *i2SStatus
 * Returns:     None
 * Description: Starts an I2C read transfer with a 7bit data address.
 **************************************************************************************************/
extern void i2c_vRead(uint8_t slaveAdd, uint8_t slaveReg, uint8_t nOfB);

/************************************************************************************ i2c_vWrite ***
 * Name:        Write Data
 * Parameters:  slaveAdd
 *              slaveReg
 *              buf[]
 *              nOfB
 *              *i2SStatus
 * Returns:     None
 * Description: Starts an I2C read transfer with a 7bit data address.
 **************************************************************************************************/
extern void i2c_vWrite(uint8_t slaveAdd, uint8_t slaveReg, const uint8_t buf[], uint8_t nOfB);

#endif /* I2C_H */
/*
 * End of Workfile: main.c
 */
