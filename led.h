/***************************************************************************************************
 * Name:    TLC59116 Driver Header
 * File:    led.h
 * Author:  RDTek
 * Date:    15/11/15
 *
 * Copyright:
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 15/11/15 | RDTek | Created.
 *
 * Description:
 **************************************************************************************************/

#ifndef LED_H
#define LED_H
/**************************************************************************************** Include */
#include <xc.h>
#include <stdbool.h>
#include <stdint.h>

/**************************************************************************************** Defines */
/* I2C */
#define LED_I2C_BASE_ADD    0xC0u

/* Registers */
#define LED_MODE1_REG   0x00u
#define LED_MODE2_REG   0x01u
#define LED_PWM0_REG    0x02u
#define LED_PWM1_REG    0x03u
#define LED_PWM2_REG    0x04u
#define LED_PWM3_REG    0x05u
#define LED_PWM4_REG    0x06u
#define LED_PWM5_REG    0x07u
#define LED_PWM6_REG    0x08u
#define LED_PWM7_REG    0x09u
#define LED_PWM8_REG    0x0Au
#define LED_PWM9_REG    0x0Bu
#define LED_PWM10_REG   0x0Cu
#define LED_PWM11_REG   0x0Du
#define LED_PWM12_REG   0x0Eu
#define LED_PWM13_REG   0x0Fu
#define LED_PWM14_REG   0x10u
#define LED_PWM15_REG   0x11u
#define LED_GRPPWM_REG  0x12u
#define LED_GRPFREQ_REG 0x13u
#define LED_LEDOUT0_REG 0x14u
#define LED_LEDOUT1_REG 0x15u
#define LED_LEDOUT2_REG 0x16u
#define LED_LEDOUT3_REG 0x17u
#define LED_SUBADR0_REG 0x18u
#define LED_SUBADR1_REG 0x19u
#define LED_SUBADR2_REG 0x1Au
#define LED_ALLCALLADR_REG 0x1Bu
#define LED_IREF_REG    0x1Cu
#define LED_EFLAG1      0x1Du
#define LED_EFLAG2      0x1Eu

#define LED_MODE_ACKCHG 0x80u
#define LED_MODE_DIM    0x00u
#define LED_MODE_BLINK  0x20u
#define LED_MODE_ERRDIS 0x80u
#define LED_MODE_ERREN  0x00u
#define LED_MODE_STPCHG 0x00u

#define LED_GRPPWM      0x00u
#define LED_MAX_BRIGHT  0xFFu
#define LED_MODE_NORMAL 0x00u
#define LED_NO_AUTO     0x00u
#define LED_AUTO_ALL    0x80u

#define LED_AUTO_INC_1  0x80u
#define LED_AUTO_INC_2  0xA0u
#define LED_AUTO_INC_3  0xC0u
#define LED_AUTO_INC_4  0xE0u

#define LED_PWM_ON      255u
#define LED_PWM_OFF     0u
/******************************************************************************** Global TypeDefs */

/******************************************************************************* Global Constants */

/******************************************************************************* Global Variables */

/******************************************************************* Callback Function Prototypes */

#endif /* LED_H */

